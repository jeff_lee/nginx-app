all:
	docker build -t deis/nginx-app .

run:
	docker run -d -P deis/nginx-app
