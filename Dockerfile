FROM ubuntu:latest

RUN apt-get update && apt-get install -y nginx

ADD index.html /usr/share/nginx/html/index.html
RUN chmod 0644 /usr/share/nginx/html/index.html

VOLUME ["/var/cache/nginx"]

#EXPOSE 80 443
EXPOSE 80

CMD ["nginx", "-g", "daemon off;"]
